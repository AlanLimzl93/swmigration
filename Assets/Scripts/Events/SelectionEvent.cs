﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DragonBones;


public class SelectionEvent : BaseEvent
{
    public bool Must = false;
    public GameObject Selected = null;

    public void Test()
    {
        Debug.Log("Damage!");
        Complete();
    }

    public override void Invoke()
    {
        if (Selected && Must == false)
        {
            //var db = Selected.GetComponent<UnityArmatureComponent>();
            //db.animation.PlayWithCallback("Attack", 1, Test, true);
        }
    }
}