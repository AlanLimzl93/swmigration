﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEvent
{
    public bool completed = false;

    public virtual void Invoke() { }
    public void Complete() { completed = true; }
}

public class CustomEvent
{
    public Queue<BaseEvent> EventQueue = new Queue<BaseEvent>();

    public bool Invoke()
    {
        if (EventQueue.Count > 0)
        {
            var ev = EventQueue.Peek();
            ev.Invoke();
            if(ev.completed)
            {
                EventQueue.Dequeue();
            }
            return false;
        }
        return true;
    }
}




