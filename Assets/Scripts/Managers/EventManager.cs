﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{

    public List<CustomEvent> Events = new List<CustomEvent>();


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Events.Count > 0)
        {
            if (Events[0].Invoke())
                Events.RemoveAt(0);
        }
    }
        

    public void Test()
    {
        var ev = new CustomEvent();
        var s = new SelectionEvent();
        s.Selected = GameObject.Find("TyrantDreadDeath");
        ev.EventQueue.Enqueue(s);
        ev.EventQueue.Enqueue(new BuffEvent());

        Events.Add(ev);
        Debug.Log("Add Event");
    }
}
