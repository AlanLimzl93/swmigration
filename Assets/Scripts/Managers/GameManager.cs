﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public int m_Gameturn;
    public int m_Userturn;

    public delegate void GMCallback();
    public GMCallback m_GMCallback;
    public GAMESTATE m_GameState;

    //[Header("Objects")]
    //public GameObject

    public delegate void OnStateChangeHandler();
    public enum GAMESTATE
    {
        PLACECHAMPION = 0,
        RECKONING = 1,
        COINTOSS = 2,
        CARDDRAW = 3,
        GAMEPLAY = 4,
        ENDTURN = 5
    }

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
        m_Gameturn = 0;
        m_Userturn = 0; // 0 - Player, 1 - Opponent
        m_GameState = GAMESTATE.PLACECHAMPION;
        //EndTurn();


    }

    private void Update()
    {
    }

    public int Cointoss()
    {
        return Random.Range(0, 2);
    }

    public void EndTurn()
    {
        m_Gameturn++;
        m_Userturn = (m_Userturn + 1) % 2;
        EndTurnCallback(EndTurnAnimation);
    }


    public void EndTurnAnimation()
    {
        Debug.Log("Animation Starts...");
        Debug.Log("GameTurn: " + m_Gameturn);
        Debug.Log("UserTurn: " + m_Userturn);
    }

    public void EndTurnCallback(GMCallback _Callback)
    {
        m_GMCallback = _Callback;
        // delay for 3 secs
        Invoke("EndTurnCallbackAction", 0);
    }

    public void EndTurnCallbackAction()
    {
        m_GMCallback();
    }
}
