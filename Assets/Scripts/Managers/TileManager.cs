﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TileManager : MonoBehaviour
{
    public static TileManager Instance;

    public GameObject PlayerTileParent;
    public GameObject OpponentTileParent;

    private List<TileBehaviour> PlayerTiles = new List<TileBehaviour>();
    private List<TileBehaviour> OpponentTiles = new List<TileBehaviour>();

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        // Checks if Player's Tile Parent is valid object
        if(PlayerTileParent)
        {
            foreach (Transform child in transform)
                PlayerTiles.Add(child.gameObject.GetComponent<TileBehaviour>());
        }

        // Checks if Opponent's Tile Parent is valid object
        if (OpponentTileParent)
        {
            foreach (Transform child in transform)
                OpponentTiles.Add(child.gameObject.GetComponent<TileBehaviour>());
        }
    }
}
