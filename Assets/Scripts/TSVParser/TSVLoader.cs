﻿using UnityEngine;

public abstract class TSVLoader : MonoBehaviour
{
	public abstract void Load();
}
