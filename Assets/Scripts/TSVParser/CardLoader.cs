﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class CardLoader : TSVLoader
{
	private List<SO_Cards> cardArr;
	private const string FilePath = "Assets/Datasheets/Cards.tsv";
	private const int NumberOfLinesToSkip = 1;

	public override void Load()
	{
		cardArr = new List<SO_Cards>(Resources.LoadAll<SO_Cards>("ScriptableObjects/Cards"));

		StreamReader sr = new StreamReader(FilePath);

		string readLine = "";
		int line = 0;

		SO_Cards currentCard = null;

		while ((readLine = sr.ReadLine()) != null)
		{
			if (line++ < NumberOfLinesToSkip) continue;
			string[] v = readLine.Split('\t');

			if (v[0] != "")
			{
				var card = cardArr.Find((c) => c.name == v[0]);
				currentCard = card;

				card.m_Type = int.Parse(v[1]);
				card.m_CardName = v[3];
				card.m_Rarity = int.Parse(v[4]);
				card.m_Race = int.Parse(v[5]);

				card.m_Ori = v[7];
				card.m_Dialog = v[8];
				card.m_Background = v[9];
				card.m_Animation = v[10];
				card.m_Artist = v[11];

				card.m_SummonCost = int.Parse(v[12]);
				card.m_Attack = int.Parse(v[13]);
				card.m_Life = int.Parse(v[14]);

				card.m_Ablities = new List<Ability>(0);
				card.m_Ablities.Add(CreateNewAbility(v));
			}
			else
			{
				if (currentCard != null)
					currentCard.m_Ablities.Add(CreateNewAbility(v));
			}

		}
	}

	private Ability CreateNewAbility(string[] v)
	{
		Ability newAbility = new Ability();

		newAbility.m_AbilityType = int.Parse(v[15]);
		newAbility.m_AbilityStr = v[16];

		newAbility.m_Ability = new KeyValuePair<int, string>(newAbility.m_AbilityType, newAbility.m_AbilityStr);

		newAbility.m_AbilityDesc = v[17];
		newAbility.m_AbilityTargetType = int.Parse(v[18]);
		newAbility.m_AbilityTargetTeam = int.Parse(v[19]);
		newAbility.m_AbilityDmg = int.Parse(v[20]);

		return newAbility;
	}

}
