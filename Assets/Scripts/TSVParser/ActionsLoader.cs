﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ActionsLoader : TSVLoader
{
	private SO_Actions obj;
	private const string FilePath = "Assets/Datasheets/Actions.tsv";
	private const int NumberOfLinesToSkip = 1;

	public override void Load()
	{
		obj = Resources.Load<SO_Actions>("ScriptableObjects/SO_Actions");

		obj.m_Names = new List<string>();
		obj.m_SoulCosts = new List<int>();
		obj.m_Descriptions = new List<string>();

		StreamReader sr = new StreamReader(FilePath);

		string readLine = "";
		int line = 0;
		while ((readLine = sr.ReadLine()) != null)
		{
			if (line++ < NumberOfLinesToSkip) continue;
			var v = readLine.Split('\t');

			obj.m_Names.Add(v[0]);
			obj.m_SoulCosts.Add(int.Parse(v[1]));
			obj.m_Descriptions.Add(v[6]);
		}
	}
}
