﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct CARDTYPE
{
	public const int
		UNIT = 0,
		CHAMPION = 1,
		SPELL = 3,
		DESTINY = 4,
		DEMIGOD = 5;
}

public struct CARDRACE
{
	public const int
		UNLIVING = 2,
		dFACTION = 3,
		ELVES = 4;
}

public struct CARDABILITY
{
	public const int
		BLADE_OF_DARKNESS = 0,
		BLOOD_TRANSFER = 1,
		CONCEAL = 2,
		CRITICAL_FINESSE = 3,
		CRUSH = 4,
		DEATHWISH = 5,
		FIERCE = 6,
		INSPIRE = 7,
		INTIMIDATE = 8,
		JUDGEMENT = 9,
		LIFESTEAL = 10,
		MANIPULATION = 11,
		MINDFLAY = 12,
		POWER_OF_THE_UNSEEN = 13,
		RAMPAGE = 14,
		RECKONING = 15,
		REGENERATION = 16,
		SWIFT = 17,
		TRAP = 18,
		TUNDRA_AGE = 19,
		VANISH = 20;
}

[System.Serializable]
public struct Ability
{
	public int m_AbilityType;
	public string m_AbilityStr;
	public KeyValuePair<int, string> m_Ability;

	public string m_AbilityDesc;
	public int m_AbilityTargetType;
	public int m_AbilityTargetTeam;
	public int m_AbilityDmg;
}

[CreateAssetMenu(menuName = "New Card")]
public class SO_Cards : ScriptableObject
{
	public int m_Type;
	public string m_CardName;
	public int m_Rarity;
	public int m_Race;

	public string m_Ori;
	public int m_SummonCost;
	public int m_Attack;
	public int m_Life;

	public List<Ability> m_Ablities;

	public string m_Dialog;
	public string m_Background;
	public string m_Animation;
	public string m_Artist;
}