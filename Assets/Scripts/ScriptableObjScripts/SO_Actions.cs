﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AllActions")]
public class SO_Actions : ScriptableObject
{
	public List<string> m_Names;
	public List<int> m_SoulCosts;
	public List<string> m_Descriptions;
}
