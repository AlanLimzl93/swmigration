﻿using UnityEngine;
using UnityEditor;

public class EditorTools : MonoBehaviour
{
	[MenuItem("Load TSV/Load Actions")]
	static void TestLoad()
	{
		TSVLoader tsv = new ActionsLoader();
		tsv.Load();
	}

	[MenuItem("Load TSV/Load Cards")]
	static void TestLoad2()
	{
		TSVLoader tsv = new CardLoader();
		tsv.Load();
	}

}
